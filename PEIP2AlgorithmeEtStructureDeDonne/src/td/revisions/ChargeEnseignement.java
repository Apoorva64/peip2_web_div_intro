package td.revisions;

public class ChargeEnseignement {
    protected final String nom;
    protected final String prenom;
    protected final int heuresService;
    public int MINIMUM_HEURES;

    public ChargeEnseignement(String nom, String prenom, int heuresService) {
        this.nom = nom;
        this.prenom = prenom;
        this.heuresService = heuresService;
    }

}
