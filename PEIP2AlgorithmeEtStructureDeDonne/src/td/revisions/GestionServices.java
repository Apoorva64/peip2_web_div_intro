package td.revisions;

import java.util.ArrayList;


public class GestionServices {
    private final ArrayList<ChargeEnseignement> list;
    private final String nom;


    public GestionServices(String nom) {
        this.list = new ArrayList<>();
        this.nom = nom;

    }

    public void add(ChargeEnseignement p) {
        list.add(p);
    }

    @Override
    public String toString() {
        StringBuilder out = new StringBuilder("Services à " + this.nom);
        out.append('\n');
        int heuresAPayer = 0;
        for (ChargeEnseignement chargeEnseignement : list) {
            heuresAPayer += chargeEnseignement.heuresService;
        }
        out.append("Total d'heures à payer ").append(heuresAPayer).append("h\n");


        for (ChargeEnseignement chargeEnseignement : this.list) {
            out.append(chargeEnseignement.toString());
            if (chargeEnseignement.heuresService - chargeEnseignement.MINIMUM_HEURES > 0) {

                out.append(", dépasse son service de ")
                        .append(chargeEnseignement.heuresService - chargeEnseignement.MINIMUM_HEURES)
                        .append("h\n");
            } else {
                out.append(", en sous service de ")
                        .append(-(chargeEnseignement.heuresService - chargeEnseignement.MINIMUM_HEURES))
                        .append("h\n");
            }
        }
        return out.toString();

    }
}
