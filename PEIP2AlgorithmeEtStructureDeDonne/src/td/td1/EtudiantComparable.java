package td.td1;

import java.util.Objects;

public class EtudiantComparable implements Comparable<EtudiantComparable> {
    Etudiant etudiant;

    public EtudiantComparable(Etudiant etudiant) {
        this.etudiant = etudiant;
    }

    @Override
    public int compareTo(EtudiantComparable o) {
        return Integer.compare(o.etudiant.getRange(), etudiant.getRange());
    }

    @Override
    public String toString() {
        return "EtudiantComparable{" +
                "etudiant=" + etudiant +
                '}';
    }

    public Etudiant getEtudiant() {
        return etudiant;
    }

    public void setEtudiant(Etudiant etudiant) {
        this.etudiant = etudiant;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EtudiantComparable that = (EtudiantComparable) o;
        return Objects.equals(etudiant, that.etudiant);
    }


    @Override
    public int hashCode() {
        return Objects.hash(etudiant);
    }
}
