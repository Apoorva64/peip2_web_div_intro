package td.td1;

public class Etudiant {
    private final String name;
    private final int range;

    public Etudiant(String n, int r) {
        name=n;
        range=r;
    }

    public int getRange() {
        return range;
    }

    public String getName() {
        return name;
    }


    @Override
    public String toString() {
        return "Etudiant{" +
                "name='" + name + '\'' +
                ", range=" + range +
                '}';
    }


}
