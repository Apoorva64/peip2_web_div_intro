package games.co;

public class HumainPlayer extends Player {
    private String nextMove;

    public HumainPlayer(HttpClient restClient, String playerName, String gameId) {
        this.restClient = restClient;
        this.playerName = playerName;
        this.gameId = gameId;
    }

    /**
     * This method will allow to setup the user's requested move, as the move() method receives no arguments.
     */
    public void setNextMove(String fromTo) {
        this.nextMove = fromTo;
    }

    /**
     * This method will request the server to move a user's piece
     *
     * @return 0 in case of success, -1 in case of server error, and
     * -2 in case of invalid move
     */
    public int move() {
        if(nextMove.length()!=4) return -2;
        restClient.createReq("POST", "/api/v1/chess/one/move/player");
        restClient.addHeaderLine("Content-Type: application/x-www-form-urlencoded");
        restClient.addHeaderLine("Connection: close");
        restClient.addBodyData("from=" + nextMove.substring(0, 2));
        restClient.addBodyData("to=" + nextMove.substring(Math.max(nextMove.length() - 2, 0)));
        restClient.addBodyData("game_id="+gameId);
        String[] response = restClient.sendRequest();

        //Once the reply has been obtained, we need to check if we
        //received a "200 OK"
        if (response[0].contains("200 OK")) {
            if (ChessGameInterface.getValueFromKeyJSON(response[1], "status").contains("expired")) return -1;
            if (ChessGameInterface.getValueFromKeyJSON(response[1], "status").contains("invalid move")) return -2;

        } else {
            //If not, return à -1
            return -1;
        }

        // in all other cases, return 0 (success code)
        return 0;
    }

}